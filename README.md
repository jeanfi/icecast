# Running icecast with Docker or Kubernetes

## Usage

### Running icecast with docker engine

Create the icecast service and bind its port to the port 8000 of the host:

```bash
docker run -p 8000:8000 --rm --name icecast jeanfi/icecast
```

Open [web interface](http://localhost:8000).

Environment variables of the docker image:

| Name             | Description                                        |
|------------------|----------------------------------------------------|
| ICECAST_HOSTNAME | DNS name or IP address of the icecast web service  |
| ICECAST_PASSWORD | Password for administration access and add source  |

### Running icecast with kubernetes

```bash
cd kubernetes
kubectl apply -k .
```

## Build image

To build the docker image locally:

```bash
cd docker
./build
```
